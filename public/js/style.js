function clearSession() {
    localStorage.removeItem('token');
    localStorage.removeItem('auth');
    this.name = localStorage.getItem('name');
    this.user_type = localStorage.getItem('user_type');
    this.auth = localStorage.getItem('auth');
    this.item = localStorage.getItem('item');
}

moveOnMax = function(field, nextFieldID) {
    if (field.value.length == 1) {
        document.getElementById(nextFieldID).focus();
    }
}