import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import VueLoading from 'vuejs-loading-plugin'
import VueTelInput from 'vue-tel-input';
import JwPagination from 'jw-vue-pagination';

Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueLoading)
Vue.use(VueToast);
Vue.use(VueTelInput);
Vue.component('jw-pagination', JwPagination);


import App from './view/App'
import Index from './pages/index'
import Shopping from './books/shopping'
import Shop_category from './books/shopping_category'
import Shop from './books/shop'
import Login from './pages/login'
import Invoice from './books/invoice'
import Orders from './pages/orders'
import Donwnload from './pages/downloads'
import Uploads from './books/uploads'
import Upload from './books/upload'
import Edit from './books/edit'
import Logs from './pages/logs'
import Profile from './pages/profile'
import Wallet from './pages/wallet'
import Register from './pages/register'

const router = new VueRouter({
    mode: 'history',
    routes: [{
            path: '/',
            name: 'index',
            component: Index
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/profile',
            name: 'profile',
            component: Profile
        },
        {
            path: '/wallet',
            name: 'wallet',
            component: Wallet
        },
        {
            path: '/logs',
            name: 'logs',
            component: Logs
        },
        {
            path: '/uploads',
            name: 'uplaods',
            component: Uploads
        },
        {
            path: '/upload',
            name: 'uplaod',
            component: Upload
        },
        {
            path: '/upload/edit/:id',
            name: 'edit',
            component: Edit
        },
        {
            path: '/shopping',
            name: 'shopping',
            component: Shopping
        },
        {
            path: '/shop/category/:id',
            name: 'shop_category',
            component: Shop_category
        },
        {
            path: '/shop/:id',
            name: 'shop',
            component: Shop
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/invoice/:id',
            name: 'invoice',
            component: Invoice
        },
        {
            path: '/orders',
            name: 'orders',
            component: Orders
        },
        {
            path: '/downloads',
            name: 'downloads',
            component: Donwnload
        },

    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,

});