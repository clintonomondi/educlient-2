<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Edubora</title>
    <!-- SEO Meta Tags-->
    <meta name="description" content="">
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Boost your Grades"/>
    <meta property="og:image" content="/images/logo2.png"/>
    <meta property="og:description" content="Edubora is an e-learning platform that connects learners with all resources they require to reach their full potential."/>

    <!-- Viewport-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon and Touch Icons-->
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/logo2.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/logo2.png">
    <link rel="manifest" href="site.webmanifest">
    <link rel="mask-icon" color="#5bbad5" href="safari-pinned-tab.svg">
    <meta name="msapplication-TileColor" content="#766df4">
    <meta name="theme-color" content="#ffffff">
    
    <!-- Vendor Styles-->
    <link rel="stylesheet" media="screen" href="/assets/vendor/simplebar/dist/simplebar.min.css"/>
    <link rel="stylesheet" media="screen" href="/assets/vendor/tiny-slider/dist/tiny-slider.css"/>
    <link rel="stylesheet" media="screen" href="/assets/vendor/flatpickr/dist/flatpickr.min.css"/>
    <!-- Main Theme Styles + Bootstrap-->
    <link rel="stylesheet" media="screen" href="/assets/css/theme.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" media="screen" href="/css/invoice.css">
    <link rel="stylesheet" media="screen" href="/css/buttona.css">
  </head>
  <!-- Body-->
  <body>


    <main class="page-wrapper">
    <div class="content2">
        <div id="app">
            <app></app>
        </div>
        </div>
        <script src="{{ mix('js/app.js') }}"></script>

    <!-- Footer-->
    <footer class="footer pt-lg-5 pt-4 bg-dark text-light">

      <div class="py-4 border-top border-light">
        <div class="container d-flex flex-column flex-lg-row align-items-center justify-content-between py-2">
          <!-- Copyright-->
          <p class="order-lg-1 order-2 fs-sm mb-2 mb-lg-0"><span class="text-light opacity-60">&copy; All rights reserved </span></p>
          <div class="d-flex flex-lg-row flex-column align-items-center order-lg-2 order-1 ms-lg-4 mb-lg-0 mb-4">
            <!-- Links-->
            <div class="d-flex flex-wrap fs-sm mb-lg-0 mb-4 pe-lg-4"><a class="nav-link-light px-2 mx-1" href="/terms.pdf">Terms</a>
              <a class="nav-link-light px-2 mx-1 fa fa-envelope" href="#">info@edubora.co.ke
              </a>
              <a class="nav-link-light px-2 mx-1 fa fa-phone" href="#">+254717791504</a>
            </div>
            <div class="d-flex align-items-center">
              <!-- Language switcher-->
              <div class="dropdown"><a class="nav-link nav-link-light dropdown-toggle fs-sm align-items-center p-0 fw-normal" href="#" id="langSwitcher" data-bs-toggle="dropdown" role="button" aria-expanded="false">
                <i class="fi-globe mt-n1 me-2 align-middle"></i>Eng</a>
                <ul class="dropdown-menu dropdown-menu-dark my-1" aria-labelledby="langSwitcher">
                  <li><a class="dropdown-item text-nowrap py-1" href="#"><img class="me-2" src="img/flags/de.png" width="20" alt="Deutsch">Deutsch</a></li>
                  <li><a class="dropdown-item text-nowrap py-1" href="#"><img class="me-2" src="img/flags/fr.png" width="20" alt="Français">Français</a></li>
                  <li><a class="dropdown-item text-nowrap py-1" href="#"><img class="me-2" src="img/flags/es.png" width="20" alt="Español">Español</a></li>
                </ul>
              </div>
              <!-- Socials-->
              <div class="ms-4 ps-lg-2 text-nowrap">
                <a class="btn btn-icon btn-translucent-light btn-xs rounded-circle ms-2" href="https://api.whatsapp.com/send?phone=+254717791504&text=Hello,Edubora, enquiry" target="_blank">
                <i class="fi-whatsapp"></i></a>
                <a class="btn btn-icon btn-translucent-light btn-xs rounded-circle ms-2" href="https://web.facebook.com/edubora.kcpe.kcse.revision" target="_blank">
                  <i class="fi-facebook"></i></a>
                  <a class="btn btn-icon btn-translucent-light btn-xs rounded-circle ms-2" href="https://www.youtube.com/channel/UCmcaTejXrwbW-LCO8lOzc9w" target="_blank">
                    <i class="fi-youtube"></i></a>
                <a class="btn btn-icon btn-translucent-light btn-xs rounded-circle ms-2" href="https://twitter.com/Edubora2" target="_blank">
                  <i class="fi-twitter"></i></a>
                  <a class="btn btn-icon btn-translucent-light btn-xs rounded-circle ms-2" href="https://www.linkedin.com/company/edubora/" target="_blank">
                    <i class="fi-linkedin"></i></a>
                    <a class="btn btn-icon btn-translucent-light btn-xs rounded-circle ms-2" href="https://www.instagram.com/edubora_academy/" target="_blank">
                      <i class="fi-instagram"></i></a></div>
            </div>
          </div>
        </div>
      </div>
    </footer>
    </main>
 
    <script src="/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/vendor/simplebar/dist/simplebar.min.js"></script>
    <script src="/assets/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>
    <script src="/assets/vendor/tiny-slider/dist/min/tiny-slider.js"></script>
    <script src="/assets/vendor/flatpickr/dist/flatpickr.min.js"></script>
    <!-- Main theme script-->
    <script src="/assets/js/theme.min.js"></script>
    <script src="/js/style.js"></script>
    

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"  ></script>

<script src="/loader/center-loader.js"></script>
  </body>
</html>